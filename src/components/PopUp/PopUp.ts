import {ref} from "@vue/reactivity"

export default {
    props: {
        html: String,
        text: String,
        data: Object,
        isAnotherDisplayed: Boolean,
    },
    setup(props: any, { emit }: any) {
        const isPersisting = ref(false)
        const isClosing = ref(false)

        const methods = {
            persistPopUp() {
                if(props.isAnotherDisplayed || isClosing.value) return
                emit('persist-request')
                isPersisting.value = true
            },
            closePopUp() {
                isClosing.value = true
                emit('close-request')
                isPersisting.value = false
            },
            resetPopUp() {
                isClosing.value = false
            }
        }

        return { isPersisting, isClosing, ...methods }
    },
}
