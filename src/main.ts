import { createApp } from 'vue'
import App from './App.vue'
import './default.css'

createApp(App).mount('#app')
