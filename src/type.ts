export interface Schedule {
    days: number[],
    week: Week[],
}

export interface Week {
    label: string,
    schedule: Meeting[],
}

export interface Meeting {
    start: string,
    end: string,
}

export interface Interval {
    from: Time,
    to: Time,
}

export interface Time {
    hour: number,
    minute: number,
}
