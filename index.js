import AppointmentSelector from './src/components/AppointmentSelector/AppointmentSelector.vue'

export const useAppointmentSelector = {
    install(app) {
        app.component('v-appointment-selector', AppointmentSelector)
    }
}

export default AppointmentSelector
