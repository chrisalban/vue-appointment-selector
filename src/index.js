import AppointmentSelector from './components/AppointmentSelector.vue'

export const install = Vue => {
    if (install.installed) return
    install.installed = true
    Vue.component('vue-appointment-selector', AppointmentSelector)
}

export const VueAppointmentSelector = {
    install,
}

let GlobalVue = null
if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue
}

if( GlobalVue ) {
    GlobalVue.use(VueAppointmentSelector)
}

export default AppointmentSelector
