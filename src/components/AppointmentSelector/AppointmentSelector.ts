import dayjs from "dayjs";
import PopUp from '../PopUp/PopUp.vue'
import { computed, onMounted, PropType, ref, watch } from 'vue'
import { Interval, Meeting, Schedule } from "../../type";

export default {
    components: { PopUp },
    props: {
        selected: {
            type: Object as PropType<Meeting>,
            default: () => {
                return {
                    start: '',
                    end: '',    
                }
            }
        },
        appointmentsTaken: {
            type: Object as PropType<Meeting[]>,
            default: () => []
        },
        intervals: {
            type: Object as PropType<Interval[]>,
            required: true
        },
        minWeeks:{
            type: Number,
            default: 1
        },
        nonWorkingDays: {
            type: Array as PropType<Number[]>,
            default: () => [ 0, 6 ]
        }, 
        appointmentDuration: {
            type: Number,
            default: 30
        },
        scheduleLabel: {
            type: String,
            default: 'No schedule has been selected'
        },
        relationLabel: {
            type: String,
            default: 'Hour / Day'
        }
    },
    emits: ['update:selected', 'cell-clicked'],
    setup(props: any, { emit }: any) {
        const selectedSchedule = computed(() => {
            if(!props.selected.start || !props.selected.end) return props.scheduleLabel

            let start = dayjs(props.selected.start)
            let end = dayjs(props.selected.end)
            
            return `${start.format('ddd DD, MMMM')} ${start.format('HH:mm')} - ${end.format('HH:mm')}`
        })

        const weeks = ref<Schedule[]>([])

        const isAnotherPopUpDisplayed = ref(false)
        
        const methods = {
            addWeek() {
                let newWeek = weeks.value.length
                    ? dayjs(weeks.value[weeks.value.length - 1].week[0].schedule[6].end).add(1, 'day')
                    : dayjs()
                
                let weeksArray = []
                let weekDays = []
                for(let w = 0; w <= 6; w++) 
                {
                    let schedule = []
                    
                    for(let workSchedule of props.intervals) {
                        newWeek = newWeek.day(w).hour(workSchedule.from.hour).minute(workSchedule.from.minute).second(0)
                        let currentSchedule = true

                        while(currentSchedule) {
                            let start = newWeek.format('YYYY-MM-DD HH:mm:ss')
                            newWeek = newWeek.add(props.appointmentDuration, 'minute')
                            let end = newWeek.format('YYYY-MM-DD HH:mm:ss')

                            schedule.push({ start, end })
                            if(newWeek.hour() === workSchedule.to.hour && newWeek.minute() === workSchedule.to.minute) currentSchedule = false
                        }
                    }
                    weekDays.push(newWeek.date())
                    weeksArray.push(schedule)
                }

                let schedule: Schedule = {
                    days: [],
                    week: []
                }
                for(let s = 0; s < weeksArray[0].length ; s++)
                {
                    let week = []
                    for(let w = 0; w <= 6; w++)
                    {
                        week.push(weeksArray[w][s])
                    }
                    schedule.week.push({
                        label: `${weeksArray[0][s].start.split(' ')[1].substring(-3, 5)} - ${weeksArray[0][s].end.split(' ')[1].substring(-3, 5)}`, 
                        schedule: week
                    })
                }
                schedule.days = weekDays 
                weeks.value.push(schedule)
            },
            setMeeting(meeting: Meeting) {
                if(methods.isDisabled(meeting) || isAnotherPopUpDisplayed.value) return
                emit('update:selected', meeting)
                emit('cell-clicked', meeting)
            },
            scrolled(e: any) {
                if (e.target.offsetHeight + e.target.scrollTop >= e.target.scrollHeight) {
                    methods.addWeek()
                } 
            },
            isSelected(meeting: Meeting) {
                return props.selected.start === meeting.start && props.selected.end === meeting.end
            },
            isDisabled(meeting: Meeting) {
                let start = dayjs(meeting.start)
                return dayjs().isAfter(start)
                    || props.nonWorkingDays.includes(start.day())
                    || methods.getAppointmentTaken(meeting.start, meeting.end) !== undefined
            },
            getAppointmentTaken(start: string, end: string)
            {
                return props.appointmentsTaken.find( (appointment: Meeting) => appointment.start === start && appointment.end === end )
            },
            getDayLabel(dayOfWeek: number) {
                return dayjs().day(dayOfWeek).format('dddd')
            },
            initWeeks()
            {
                let min = props.minWeeks
                do {
                    methods.addWeek()
                    min--
                } while(min > 0)
            },
            showPopUpInfo(schedule: Meeting)
            {
                let appointment = methods.getAppointmentTaken(schedule.start, schedule.end)
                if(appointment === undefined) return false
                return appointment.text !== undefined || appointment.html !== undefined || appointment.data !== undefined
            },
            getTextFromAppointment(schedule: Meeting)
            {
                let appointment = methods.getAppointmentTaken(schedule.start, schedule.end)
                if(appointment === undefined) return undefined
                return appointment.text
            },
            getHtmlFromAppointment(schedule: Meeting)
            {
                let appointment = methods.getAppointmentTaken(schedule.start, schedule.end)
                if(appointment === undefined) return undefined
                return appointment.html
            },
            getDataFromAppointment(schedule: Meeting)
            {
                let appointment = methods.getAppointmentTaken(schedule.start, schedule.end)
                if(appointment === undefined) return undefined
                return appointment.data
            },
        }

        watch([props.intervals, () => props.appointmentDuration, () => props.appointmentsTaken, () => props.minWeeks, () => props.nonWorkingDays, ], () => methods.initWeeks())

        onMounted(() => {
            methods.initWeeks()
        }) 

        return { selectedSchedule, weeks, isAnotherPopUpDisplayed, ...methods }
    },
}
